# Js Notes App CLI

> A simple app to (add remove list read) notes using command line

## Run It On Your Machine
```sh
$ git clone git@gitlab.com:SafeMood/nodenotesapp.git
```
```sh
$ cd nodenotesapp
```
```sh
$ npm install 
```

## Usage
### List all Note
```sh
$  node app list
```

### Add a Note
```sh
$  node app add --title="Nodejs" --body="Is fantastic"
```
### Read a Note
```sh
$  node app read --title="Nodejs"
```
### Remove a Note
```sh
$  node app remove --title="Nodejs"
```

## License

MIT License
